import Token from './Token'
import AppStorage from './AppStorage'

class User{

  responseAfterLogin(res)
  {
    const access_token = res.data.access_token
    const username = res.data.name
    const role = res.data.role
    const user_id = res.data.user_id

    // untuk buat admin dan user UI boleh buat sini
    if(Token.isValid(access_token))
    {
      AppStorage.store(access_token, username, role, user_id)
    }
  }


  hasToken(){
    const storeToken = localStorage.getItem('token');
    const username = localStorage.getItem('token');
    if(storeToken){
      return Token.isValid(storeToken) ? true : false
    }
     false
  }

  checkRole(){
    const storeToken = localStorage.getItem('token');
    const storeRole = localStorage.getItem('role');

    if((storeToken) && (storeRole == 'admin')){
      return Token.isValid(storeToken) ? true : false
    }
    false
  }

  loggedIn()
  {
    return this.hasToken() 
    // return this.checkRole() 
  }

  // loggedCheck(){
  //   return this.checkRole() 
  // }

  name(){
    if(this.loggedIn()){
        return localStorage.getItem('user')
    }
  }

  id(){
    if(this.loggedIn()){
        const payload = Token.payload(localStorage.getItem('token'));
        return payload.sub
    }
  }

  todos(){
    if(this.loggedIn()){
       return localStorage.getItem('user_id')
    }
  }

  }
  
  export default User = new User();