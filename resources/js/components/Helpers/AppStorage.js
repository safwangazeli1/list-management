class AppStorage{

  storeToken(token){
    localStorage.setItem('token', token);
  }

  storeUser(user){
    localStorage.setItem('user', user);
  }

  storeUser_id(user_id){
    localStorage.setItem('user_id', user_id);
  }

  storeRole(role){
    localStorage.setItem('role', role);
  }

  store(token,user,role,user_id){
    this.storeToken(token)
    this.storeUser(user)
    this.storeRole(role)
    this.storeUser_id(user_id)
  }

  clear(){
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    localStorage.removeItem('role')
    localStorage.removeItem('user_id')
  }

  getToken(){
    localStorage.getItem(token);
  }

  getUser(){
    localStorage.getItem(user);
  }

  getRole(){
    localStorage.getItem(role);
  }

  getUser_id(){
    localStorage.getItem(user_id);
  }
}

export default AppStorage = new AppStorage();