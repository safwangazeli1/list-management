
let login = require('./components/auth/login.vue').default;
let forget = require('./components/auth/forget.vue').default;
let logout = require('./components/auth/logout.vue').default;


// todos
let storetodos = require('./components/todolist/create.vue').default;
let todos = require('./components/todolist/index.vue').default;
let edittodos = require('./components/todolist/edit.vue').default;

// admin
let register = require('./components/admin/register.vue').default;
let admin = require('./components/admin/index.vue').default;
let editadmin = require('./components/admin/edit.vue').default;



export const routes = [
    { path: '/', component: login, name: '/' },
    { path: '/forget', component: forget, name: 'forget' },
    { path: '/logout', component: logout, name: 'logout' },

    // employee route
    { path: '/store-todos', component: storetodos, name: 'store-todos' },
    { path: '/todos', component: todos, name: 'todos' },
    { path: '/edit-todos/:id', component: edittodos, name: 'edit-todos' },

    // admin
    { path: '/admin', component: admin, name: 'admin' },
    { path: '/edit-admin/:id', component: editadmin, name: 'edit-admin' },
    { path: '/register', component: register, name: 'register' },

      ]