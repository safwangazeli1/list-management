<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TodosController;
use App\Http\Controllers\Api\FileUploadsController;


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login',  [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('signup', [AuthController::class, 'signup']);
    
});

Route::ApiResource('/todos', TodosController::class);

Route::ApiResource('/user', UserController::class);

Route::patch('/active/{id}', [TodosController::class, 'completeTask']);

