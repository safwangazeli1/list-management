<?php

namespace App\Models\Model;

use App\Models\Model\FileUploads;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Todos extends Model
{

    use HasFactory;
    protected $fillable = [
        'message',
        'is_complete',
        'user_id',
    ];

    
}