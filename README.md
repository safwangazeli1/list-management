# List Management 
To run this application you have to clone this repo from the instructions below:

### Clone: 

```git clone --single-branch --branch main https://gitlab.com/safwangazeli1/list-management.git```


### Install : 

Then you need to install: 

```composer install```


After finishing all the steps above, you need to add a new .env file and set up your database name. Then run to get all the tables and generate App Key using:

```php artisan key:generate```

```php artisan migrate```

```php artisan db:seed```

After the run command above, you must use this email and password to login: 
email: admin@gmail.com
password: admin

```php artisan jwt:secret```

```npm install```

then, run the system in localhost using this command:

```php artisan serve```

```npm run watch```
